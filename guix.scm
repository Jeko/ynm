(use-modules
 (jeko-packages)
 (gnu packages guile)
 (gnu packages guile-xyz)
 (gnu packages code)
 (gnu packages tls)
 (guix packages)
 (guix build-system guile)
 ((guix licenses) #:prefix license:)
 (guix git-download)
 (guix gexp)
 (ice-9 popen)
 (ice-9 rdelim))

(define source-dir
  (dirname (current-filename)))
(define commit
  (read-string (open-pipe "git show HEAD | head -1 | cut -d ' ' -f 2" OPEN_READ)))
(define revision
  "0")

(define-public ynm
  (package
    (name "ynm")
    (version (string-append (git-version "0.1.0" revision commit) "-HEAD"))
    (build-system guile-build-system)
    (source
     (local-file
      source-dir
      #:recursive? #t
      ;;#:select? (not (basename ".git"))
      ))
    (inputs (list guile-3.0))
    (propagated-inputs (list gnutls guile-json-4 guile-sqlite3))
    (arguments
     '(#:phases
       (modify-phases %standard-phases
	 (add-after 'unpack 'move-src-files
	   (lambda _
	     (delete-file "guix.scm")
	     #t)))))
    (home-page "https://framagit.org/Jeko/ynm")
    (synopsis "Your Next Meal")
    (description "YNM software helps you to eat better." )
    (license license:gpl3+)))

ynm
