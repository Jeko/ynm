((scheme-mode
  .
  ((eval . (put 'call-with-sqlite-connection 'scheme-indent-function 1))
     (eval . (put 'with-prepared-statement 'scheme-indent-function 1))
     (eval . (put 'test-error 'scheme-indent-function 1)))))
