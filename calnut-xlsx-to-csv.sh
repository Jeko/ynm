#!/usr/bin/env bash

# https://www.data.gouv.fr/fr/datasets/table-de-composition-nutritionnelle-des-aliments-ciqual/
# https://ciqual.anses.fr/#/cms/download/node/20

readonly FRENCH="FR"
readonly ENGLISH="ENG"

readonly LANG=$FRENCH

SOURCE="https://ciqual.anses.fr/cms/sites/default/files/inline-files/CALNUT2020_2020_07_07.xlsx"
#SOURCE="https://ciqual.anses.fr/cms/sites/default/files/inline-files/Table%20Ciqual%202020_${LANG}_2020%2007%2007.xls"

CALNUT_XLS="calnut.xls"
CALNUT_CSV="calnut.csv"

wget $SOURCE -O $CALNUT_XLS

WGET_RETURN_CODE=$?

if [ ${WGET_RETURN_CODE} -eq 0 ]
then
    rm $CALNUT_CSV
    libreoffice --headless --convert-to csv:"Text - txt - csv (StarCalc)":59,34,76 ${CALNUT_XLS} --outdir . > /dev/null
    #iconv -f cp1252 -t utf-8 ${CALNUT_CSV} > /dev/null
    ICONV_RETURN_CODE=$?
    if [ ${ICONV_RETURN_CODE} -eq 0 ]
    then
	sed -i 's/œ/oe/g' calnut.csv 
	sed -i 's/…/.../g' calnut.csv
	rm ${CALNUT_XLS}
	echo ${CALNUT_CSV}
    else
	echo "Cannot convert ressource encoding"
    fi
else
    echo "Cannot retrieve ressource : $SOURCE"
fi
