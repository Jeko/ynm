(define-module (tests ynm)
  #:use-module (srfi srfi-64)
  #:use-module (sqlite3)
  #:export ())

(use-modules (ynm entities food)
	     (ynm entities intake)
	     (ynm entities quantity)
	     (ynm use-cases gourmet interactors)
	     (ynm use-cases gourmet controllers)
	     (ynm use-cases gourmet presenters)
	     (ynm adapters intake-gateway-in-memory)
	     (ynm adapters food-gateway-in-memory)
	     (ynm utils csv)
	     (ynm utils calnut))

(module-define! (resolve-module '(srfi srfi-64))
		'test-log-to-file #f)

((lambda ()

   (let* ([dummy-composition (make-composition 1410 340 46.2 1.28 510 29.2 454 122 626 0.028 0.27 0.13 2.87 7.75 23.1 21.8 0.56 0.27 0.071 0.072 0.32 0.071 0.072 0.075 0.0075 0 0 27.4 18.2 6.41 0.8 0.9 0.6 0.41 0.86 0.92 3.02 7.98 2.52 5.02 0.41 0.13 0.021 0.0079 0.006 226 78 0.29 0.55 1.73 9.36 0.057 0.035 0.41 1 0.51 0.11 1.36 34.4 0 0.57 89.8)]
	  [dummy-food (make-food "12999" "Fromage (aliment moyen)" "MB" dummy-composition "05" "produits laitiers et assimilés" "0503" "fromages et assimilés" "000000" "-")]
	  [dummy-food-name (food-label dummy-food)]
	  [dummy-quantity-value "1"]
	  [dummy-quantity-unit "g"]
	  [dummy-food-intake (make-intake dummy-food-name
					  (make-quantity (string->number dummy-quantity-value)
							 (string->symbol dummy-quantity-unit)))]
	  [dummy-dietary-reference-intake (apply make-composition (make-list 62 42))]

	  [food-gateway (make-food-gateway-in-memory)]
	  [intake-gateway (make-intake-gateway-in-memory)]
	  
	  [search-presenter (make-search-food-name-presenter (const #f))]
	  [search-interactor (make-search-food-name-interactor food-gateway search-presenter)]
	  [search-food-name (make-search-food-name-controller search-interactor)]

	  [record-presenter (make-record-intake-presenter (const #f))]
	  [record-interactor (make-record-intake-interactor food-gateway intake-gateway record-presenter)]
	  [record-intake (make-record-intake-controller record-interactor)]

	  ;; « Clean Architecture - A Craftsman’s Guide to Software Structure and Design »
	  ;; Fig. 8.2  Partitioning the processes into classes and separating the classes into components
	  ;; fig. 22.2 A typical scenario for a web-based Java system utilizing a database
	  ;;;; L'Interactor implemente l'Input Boundary Interface.
	  ;;;; L'Interactor est donc injecté dans Controller.
	  ;;;; Idem pour le Presenter

	  ;; Si je considère que ma View c'est mon test, alors le Presenter me donne la bonne donnée à comparer View Model.
	  ;; Je devrais pouvoir voir des trucs genre
	  [balance-presenter (make-balance-nutrition-presenter-for-test)]
	  [balance-interactor (make-balance-nutrition-interactor intake-gateway food-gateway)]
	  [balance-controller (make-balance-nutrition-controller balance-interactor balance-presenter)]
	  )

     (for-each
      (lambda (item)
	((food-gateway-create food-gateway) (calnut->food item)))
      (csv-body "tests/assets/calnut-test.csv"))

     (test-begin "actor--gourmet")

     (test-group "search-food-name"

       (test-assert "Given-empty-food-name-Should-find-nothing-When-search-food-names"
	 (null? (search-food-name "")))
       

       (test-assert "Given-full-food-name-Should-find-the-name-When-search-food-names"
	 (begin
	   (search-food-name "Fromage (aliment moyen)")
	   (equal? '("Fromage (aliment moyen)")
		   (search-food-name-presenter-view-model))))

       (test-assert "Given-beginning-of-a-name-Should-find-all-names-starting-with-When-search-food-names"
	 (begin
	   (search-food-name "Légume")
	   (equal? '("Légume sec, cuit (aliment moyen)"
		     "Légume cuit (aliment moyen)")
		   (search-food-name-presenter-view-model))))

       (test-assert "Given-end-of-a-name-Should-find-all-names-ending-with-When-search-food-names"
	 (begin
	   (search-food-name "(aliment moyen)")
	   (equal? '("Fromage (aliment moyen)"
		     "Viande cuite (aliment moyen)"
		     "Crudité, sans assaisonnement (aliment moyen)"
		     "Fruit cru (aliment moyen)"
		     "Légume sec, cuit (aliment moyen)"
		     "Légume cuit (aliment moyen)")
		   (search-food-name-presenter-view-model))))

       (test-assert "Given-part-of-a-name-Should-find-all-names-containing-When-search-food-names"
	 (begin
	   (search-food-name "cuit")
	   (equal? '("Viande cuite (aliment moyen)"
		     "Légume sec, cuit (aliment moyen)"
		     "Légume cuit (aliment moyen)")
		   (search-food-name-presenter-view-model))))

       (test-assert "Given-full-name-with-altered-case-Should-find-the-name-When-search-food-names"
	 (begin
	   (search-food-name "fROMAGE (ALIMENT moYen)")
	   (equal? '("Fromage (aliment moyen)")
		   (search-food-name-presenter-view-model)))))
     
     (test-group "record-intake"

       (define (then-intake-is-recorded intake)
	 (member intake ((intake-gateway-read intake-gateway))))
       
       (define (then-no-intake-recorded)
	 (null? ((intake-gateway-read intake-gateway))))

       (define (then-recording-status-is status)
	 (eq? status (record-intake-presenter-view-model)))
       
       (test-assert "get-confirmation-when-intake-is-recorded"
	 (begin
	   (reset-intake-gateway-store)
	   (record-intake dummy-food-name
			  dummy-quantity-value
			  dummy-quantity-unit)
	   (and (then-intake-is-recorded dummy-food-intake)
		(then-recording-status-is 'recorded))))

       (test-assert "get-warned-if-intake-is-not-recorded-because-of-unknown-food-name"
	 (begin
	   (reset-intake-gateway-store)
	   (record-intake "unknown food" "1" "g")
	   (and (then-no-intake-recorded)
		(then-recording-status-is 'unknown-food))))

       (test-assert "get-warned-if-intake-is-not-recorded-because-of-partial-food-name"
	 (begin
	   (reset-intake-gateway-store)
	   (record-intake "(aliment moyen)" "1" "g")
	   (and (then-no-intake-recorded)
		(then-recording-status-is 'unknown-food)))))

     (test-group "intakes->composition"

       (test-assert "Given-no-intake-recorded-and-dummy-dietary-reference-intake"
	 (begin
	   (reset-intake-gateway-store)
	   (equal?
	    (composition-minus null-composition dummy-dietary-reference-intake)
	    (balance-interactor dummy-dietary-reference-intake))))

       (test-assert "dummy-dietary-reference-intake-and-dummy-intake"
	 (begin
	   (reset-intake-gateway-store)
	   (record-intake dummy-food-name dummy-quantity-value dummy-quantity-unit)
	   (equal?
	    (composition-minus (food-composition dummy-food) dummy-dietary-reference-intake)
	    (balance-interactor dummy-dietary-reference-intake)))))
     )
   
   (test-end "actor--gourmet")))

;; https://karvozavr.github.io/clean-architecture-practical-aspects/
;; https://joebew42.github.io/2021/06/05/clean-architecture-presenters-dilemma/
