#!/usr/bin/env sh
exec guile -L . -e main -s "$0" "$@"
!#

(use-modules (ice-9 getopt-long)
	     (ynm utils csv)
	     (ynm utils calnut)
	     (ynm adapters food-gateway-in-memory)
	     (ynm use-cases gourmet controllers)
	     (ynm use-cases gourmet interactors)
	     (ynm use-cases gourmet presenters)
	     (ynm use-cases gourmet views))

(define (main args)

  (define food-gateway (make-food-gateway-in-memory))
  
  (define search-view (make-search-food-names-view-cli))
  (define search-presenter (make-search-food-names-presenter search-view))
  (define search-interactor (make-search-food-names-interactor food-gateway search-presenter))
  (define search-food-names (make-search-food-names-controller search-interactor))

  (for-each
   (lambda (item)
     ((food-gateway-create food-gateway) (calnut->food item)))
   (csv-body "calnut.csv"))
  
  (let* ((option-spec '((version (single-char #\v) (value #f))
			(help    (single-char #\h) (value #f))
			(search  (single-char #\s) (value #t))
			(record  (single-char #\r) (value #t))))
	 (options (getopt-long args option-spec))
	 (help-wanted (option-ref options 'help #f))
	 (version-wanted (option-ref options 'version #f))
	 (search-wanted (option-ref options 'search #f))
	 (record-wanted (option-ref options 'record #f)))

    (cond ((or version-wanted help-wanted)
	   (begin
	     (if version-wanted
		 (display "YNM! v0.0.1\n"))
	     (if help-wanted
		 (display "\
Usage: main [options] [INPUT]
  -v, --version    Display version
  -h, --help       Display this help
  -s, --search     Search for INPUT
"))))
	  (search-wanted
	   (begin
	     (format #t "Results for: ~A\n\n" search-wanted)
	     (search-food-names search-wanted)
	     (map (lambda (item)
		    (display item)
		    (newline))
		  (search-food-names-presenter-view-model))))
	  (record-wanted
	   (display "Recorded !")))))

main
