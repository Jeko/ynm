(define-module (ynm use-cases gourmet controllers)
  #:use-module (ynm entities intake)
  #:use-module (ynm entities quantity)
  #:use-module (ynm use-cases gourmet presenters)
  #:export (make-record-intake-controller
	    make-search-food-name-controller
	    make-balance-nutrition-controller))

(define (make-record-intake-controller interactor)
  (lambda (food-label food-quantity food-quantity-unit)
    (interactor (make-intake food-label
			     (make-quantity (string->number food-quantity)
					    (string->symbol food-quantity-unit))))))

(define (make-search-food-name-controller interactor)
  (lambda (name)
    (interactor name)))

(define (make-balance-nutrition-controller interactor presenter)
  (lambda (dietary-reference-intake)
    (interactor dietary-reference-intake)))
