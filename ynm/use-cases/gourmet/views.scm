(define-module (ynm use-cases gourmet views)
  #:export (make-search-food-names-view-cli))

(define (make-search-food-names-view-cli)
  (lambda (view-model)
    (map (lambda (item)
	   (display item)
	   (newline))
	 view-model)))
