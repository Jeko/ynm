(define-module (ynm use-cases gourmet interactors)
  #:use-module (ynm entities intake-gateway)
  #:use-module (ynm entities food-gateway)
  #:use-module (ynm entities intake)
  #:use-module (ynm entities food)
  #:use-module (srfi srfi-1)
  #:export (make-record-intake-interactor
	    make-search-food-name-interactor
	    make-balance-nutrition-interactor))

(define (make-record-intake-interactor food-gateway intake-gateway output-interface)

  (define (unknown-food? intake)
    (not (equal? 1
		 (length (filter
			  (lambda (food) (string=? "MB" (food-hypoth food)))
			  ((food-gateway-read food-gateway) (intake-food-name intake)))))))
  
  (lambda (intake)
    (if (unknown-food? intake)
	(output-interface 'unknown-food)
	(begin
	  ((intake-gateway-create intake-gateway) intake)
	  (output-interface 'recorded)))))

(define (make-search-food-name-interactor food-gateway output-interface)
  (lambda (name)
    (if (string-null? name)
	'()
	(output-interface (delete-duplicates
			   (map
			    food-label
			    ((food-gateway-read food-gateway) name)))))))

(define (make-balance-nutrition-interactor intake-gateway food-gateway)  
  (lambda (dri)
    (composition-minus
     (if (null? ((intake-gateway-read intake-gateway)))
	 null-composition
	 (food-composition
	  (car ((food-gateway-read food-gateway)
		(intake-food-name (car ((intake-gateway-read intake-gateway))))))))
     dri)))
