(define-module (ynm use-cases gourmet presenters)
  #:export (make-record-intake-presenter
	    record-intake-presenter-view-model
	    make-search-food-name-presenter
	    search-food-name-presenter-view-model))

(define record-intake-view-model #f)

(define (record-intake-presenter-view-model)
  record-intake-view-model)

(define (make-record-intake-presenter view)
  (lambda (record-status)
    (set! record-intake-view-model record-status)))

(define search-result-view-model '())

(define (search-food-name-presenter-view-model)
  search-result-view-model)

(define (make-search-food-name-presenter view)
  (lambda (search-result)
    (begin
      (set! search-result-view-model search-result)
      (view search-result))))
