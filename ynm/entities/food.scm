(define-module (ynm entities food)
  #:use-module (srfi srfi-9)
  #:export (make-food
	    food?
            food-code
	    food-label
	    food-hypoth
	    food-composition
	    food-group-code
	    food-group-name-fr
	    food-subgroup-code
	    food-subgroup-name-fr
	    food-subsubgroup-code
	    food-subsubgroup-name-fr
	    make-composition
	    composition?
	    nrj-kj
	    nrj-kcal
	    water
	    salt
	    sodium
	    magnesium
	    phosphorus
	    potassium
	    calcium
	    manganese
	    iron
	    copper
	    zinc
	    selenium
	    iodine
	    proteins
	    carbohydrate
	    sugar
	    fructose
	    galactose
	    lactose
	    glucose
	    maltose
	    sucrose
	    starch
	    polyols
	    fibers
	    lipids
	    saturated-fatty-acids
	    monounsaturated-fatty-acids
	    polyunsaturated-fatty-acids
	    butyric-acids
	    caproic-acids
	    caprylic-acids
	    capric-acids
	    lauric-acids
	    myristic-acids
	    palmitic-acids
	    stearic-acids
	    oleic-acids
	    linoleic-acids
	    alpha-linoleic-acids
	    arachidonic-acids
	    eicosapentaenoic-acids
	    docosahexaenoic-acids
	    retinol
	    beta-carotene
	    vitamin-d
	    vitamin-e
	    vitamin-k1
	    vitamin-k2
	    vitamin-c
	    vitamin-b1
	    vitamin-b2
	    vitamin-b3
	    vitamin-b5
	    vitamin-b6
	    vitamin-b12
	    vitamin-b9
	    alcohol
	    organic-acids
	    cholesterol
	    null-composition
	    composition-minus))

(define-record-type <food>
  (make-food alim_code food_label hypoth composition alim_grp_code alim_grp_nom_fr alim_ssgrp_code alim_ssgrp_nom_fr alim_ssssgrp_code alim_ssssgrp_nom_fr)
  food?
  (alim_code food-code)
  (food_label food-label)
  (hypoth food-hypoth)
  (composition food-composition)
  (alim_grp_code food-group-code)
  (alim_grp_nom_fr food-group-name-fr)
  (alim_ssgrp_code food-subgroup-code)
  (alim_ssgrp_nom_fr food-subgroup-name-fr)
  (alim_ssssgrp_code food-subsubgroup-code)
  (alim_ssssgrp_nom_fr food-subsubgroup-name-fr))

(define-record-type <composition>
  (make-composition nrj_kj nrj_kcal eau_g sel_g sodium_mg magnesium_mg phosphore_mg potassium_mg calcium_mg manganese_mg fer_mg cuivre_mg zinc_mg selenium_mcg iode_mcg proteines_g glucides_g sucres_g fructose_g galactose_g lactose_g glucose_g maltose_g saccharose_g amidon_g polyols_g fibres_g lipides_g ags_g agmi_g agpi_g ag_04_0_g ag_06_0_g ag_08_0_g ag_10_0_g ag_12_0_g ag_14_0_g ag_16_0_g ag_18_0_g ag_18_1_ole_g ag_18_2_lino_g ag_18_3_a_lino_g ag_20_4_ara_g ag_20_5_epa_g ag_20_6_dha_g retinol_mcg beta_carotene_mcg vitamine_d_mcg vitamine_e_mg vitamine_k1_mcg vitamine_k2_mcg vitamine_c_mg vitamine_b1_mg vitamine_b2_mg vitamine_b3_mg vitamine_b5_mg vitamine_b6_mg vitamine_b12_mcg vitamine_b9_mcg alcool_g acides_organiques_g cholesterol_mg)
  composition?
  (nrj_kj nrj-kj)
  (nrj_kcal nrj-kcal)
  (eau_g water)
  (sel_g salt)
  (sodium_mg sodium)
  (magnesium_mg magnesium)
  (phosphore_mg phosphorus)
  (potassium_mg potassium)
  (calcium_mg calcium)
  (manganese_mg manganese)
  (fer_mg iron)
  (cuivre_mg copper)
  (zinc_mg zinc)
  (selenium_mcg selenium)
  (iode_mcg iodine)
  (proteines_g proteins)
  (glucides_g carbohydrate)
  (sucres_g sugar)
  (fructose_g fructose)
  (galactose_g galactose)
  (lactose_g lactose)
  (glucose_g glucose)
  (maltose_g maltose)
  (saccharose_g sucrose)
  (amidon_g starch)
  (polyols_g polyols)
  (fibres_g fibers)
  (lipides_g lipids)
  (ags_g saturated-fatty-acids)
  (agmi_g monounsaturated-fatty-acids)
  (agpi_g polyunsaturated-fatty-acids)
  (ag_04_0_g butyric-acids)
  (ag_06_0_g caproic-acids)
  (ag_08_0_g caprylic-acids)
  (ag_10_0_g capric-acids)
  (ag_12_0_g lauric-acids)
  (ag_14_0_g myristic-acids)
  (ag_16_0_g palmitic-acids)
  (ag_18_0_g stearic-acids)
  (ag_18_1_ole_g oleic-acids)
  (ag_18_2_lino_g linoleic-acids)
  (ag_18_3_a_lino_g alpha-linoleic-acids)
  (ag_20_4_ara_g arachidonic-acids)
  (ag_20_5_epa_g eicosapentaenoic-acids)
  (ag_20_6_dha_g docosahexaenoic-acids) ;; d'après https://www.afblum.be/bioafb/acidgras/acidgras.htm c'est C22 6
  (retinol_mcg retinol)
  (beta_carotene_mcg beta-carotene)
  (vitamine_d_mcg vitamin-d)
  (vitamine_e_mg vitamin-e)
  (vitamine_k1_mcg vitamin-k1)
  (vitamine_k2_mcg vitamin-k2)
  (vitamine_c_mg vitamin-c)
  (vitamine_b1_mg vitamin-b1)
  (vitamine_b2_mg vitamin-b2)
  (vitamine_b3_mg vitamin-b3)
  (vitamine_b5_mg vitamin-b5)
  (vitamine_b6_mg vitamin-b6)
  (vitamine_b12_mcg vitamin-b12)
  (vitamine_b9_mcg vitamin-b9)
  (alcool_g alcohol)
  (acides_organiques_g organic-acids)
  (cholesterol_mg cholesterol))

(define null-composition
  (apply make-composition (make-list 62 0)))

(define (composition-minus c1 c2)
  (make-composition
   (exact->inexact (- (nrj-kj c1) (nrj-kj c2)))
   (exact->inexact (- (nrj-kcal c1) (nrj-kcal c2)))
   (exact->inexact (- (water c1) (water c2)))
   (exact->inexact (- (salt c1) (salt c2)))
   (exact->inexact (- (sodium c1) (sodium c2)))
   (exact->inexact (- (magnesium c1) (magnesium c2)))
   (exact->inexact (- (phosphorus c1) (phosphorus c2)))
   (exact->inexact (- (potassium c1) (potassium c2)))
   (exact->inexact (- (calcium c1) (calcium c2)))
   (exact->inexact (- (manganese c1) (manganese c2)))
   (exact->inexact (- (iron c1) (iron c2)))
   (exact->inexact (- (copper c1) (copper c2)))
   (exact->inexact (- (zinc c1) (zinc c2)))
   (exact->inexact (- (selenium c1) (selenium c2)))
   (exact->inexact (- (iodine c1) (iodine c2)))
   (exact->inexact (- (proteins c1) (proteins c2)))
   (exact->inexact (- (carbohydrate c1) (carbohydrate c2)))
   (exact->inexact (- (sugar c1) (sugar c2)))
   (exact->inexact (- (fructose c1) (fructose c2)))
   (exact->inexact (- (galactose c1) (galactose c2)))
   (exact->inexact (- (lactose c1) (lactose c2)))
   (exact->inexact (- (glucose c1) (glucose c2)))
   (exact->inexact (- (maltose c1) (maltose c2)))
   (exact->inexact (- (sucrose c1) (sucrose c2)))
   (exact->inexact (- (starch c1) (starch c2)))
   (exact->inexact (- (polyols c1) (polyols c2)))
   (exact->inexact (- (fibers c1) (fibers c2)))
   (exact->inexact (- (lipids c1) (lipids c2)))
   (exact->inexact (- (saturated-fatty-acids c1) (saturated-fatty-acids c2)))
   (exact->inexact (- (monounsaturated-fatty-acids c1) (monounsaturated-fatty-acids c2)))
   (exact->inexact (- (polyunsaturated-fatty-acids c1) (polyunsaturated-fatty-acids c2)))
   (exact->inexact (- (butyric-acids c1) (butyric-acids c2)))
   (exact->inexact (- (caproic-acids c1) (caproic-acids c2)))
   (exact->inexact (- (caprylic-acids c1) (caprylic-acids c2)))
   (exact->inexact (- (capric-acids c1) (capric-acids c2)))
   (exact->inexact (- (lauric-acids c1) (lauric-acids c2)))
   (exact->inexact (- (myristic-acids c1) (myristic-acids c2)))
   (exact->inexact (- (palmitic-acids c1) (palmitic-acids c2)))
   (exact->inexact (- (stearic-acids c1) (stearic-acids c2)))
   (exact->inexact (- (oleic-acids c1) (oleic-acids c2)))
   (exact->inexact (- (linoleic-acids c1) (linoleic-acids c2)))
   (exact->inexact (- (alpha-linoleic-acids c1) (alpha-linoleic-acids c2)))
   (exact->inexact (- (arachidonic-acids c1) (arachidonic-acids c2)))
   (exact->inexact (- (eicosapentaenoic-acids c1) (eicosapentaenoic-acids c2)))
   (exact->inexact (- (docosahexaenoic-acids c1) (docosahexaenoic-acids c2)))
   (exact->inexact (- (retinol c1) (retinol c2)))
   (exact->inexact (- (beta-carotene c1) (beta-carotene c2)))
   (exact->inexact (- (vitamin-d c1) (vitamin-d c2)))
   (exact->inexact (- (vitamin-e c1) (vitamin-e c2)))
   (exact->inexact (- (vitamin-k1 c1) (vitamin-k1 c2)))
   (exact->inexact (- (vitamin-k2 c1) (vitamin-k2 c2)))
   (exact->inexact (- (vitamin-c c1) (vitamin-c c2)))
   (exact->inexact (- (vitamin-b1 c1) (vitamin-b1 c2)))
   (exact->inexact (- (vitamin-b2 c1) (vitamin-b2 c2)))
   (exact->inexact (- (vitamin-b3 c1) (vitamin-b3 c2)))
   (exact->inexact (- (vitamin-b5 c1) (vitamin-b5 c2)))
   (exact->inexact (- (vitamin-b6 c1) (vitamin-b6 c2)))
   (exact->inexact (- (vitamin-b12 c1) (vitamin-b12 c2)))
   (exact->inexact (- (vitamin-b9 c1) (vitamin-b9 c2)))
   (exact->inexact (- (alcohol c1) (alcohol c2)))
   (exact->inexact (- (organic-acids c1) (organic-acids c2)))
   (exact->inexact (- (cholesterol c1) (cholesterol c2)))))
