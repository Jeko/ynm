(define-module (ynm entities intake-gateway)
  #:export (make-intake-gateway
	    intake-gateway-create
	    intake-gateway-read
	    intake-gateway-update
	    intake-gateway-delete))

(use-modules (ynm entities gateway))

(define make-intake-gateway make-gateway)
(define intake-gateway-create gateway-create)
(define intake-gateway-read gateway-read)
(define intake-gateway-update gateway-update)
(define intake-gateway-delete gateway-delete)
