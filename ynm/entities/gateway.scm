(define-module (ynm entities gateway)
  #:export (make-gateway
	    gateway-create
	    gateway-read
	    gateway-update
	    gateway-delete))

(use-modules (srfi srfi-9))

(define-record-type <gateway>
  (make-gateway create read update delete)
  gateway?
  (create gateway-create)
  (read gateway-read)
  (update gateway-update)
  (delete gateway-delete))
