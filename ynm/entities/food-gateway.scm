(define-module (ynm entities food-gateway)
  #:export (make-food-gateway
	    food-gateway-create
	    food-gateway-read
	    food-gateway-update
	    food-gateway-delete))

(use-modules (ynm entities gateway))

(define make-food-gateway make-gateway)
(define food-gateway-create gateway-create)
(define food-gateway-read gateway-read)
(define food-gateway-update gateway-update)
(define food-gateway-delete gateway-delete)
