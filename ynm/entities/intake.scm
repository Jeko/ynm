(define-module (ynm entities intake)
  #:use-module (srfi srfi-9 gnu)
  #:export (make-intake
	    intake?
	    intake-food-name
	    intake-quantity
	    set-intake-food
	    set-intake-quantity))

(define-immutable-record-type <intake>
  (make-intake food-name quantity)
  intake?
  (food-name intake-food-name set-intake-food)
  (quantity intake-quantity set-intake-quantity))
