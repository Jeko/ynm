(define-module (ynm entities quantity)
  #:use-module (srfi srfi-9)
  #:export (make-quantity
	    quantity?
	    quantity-value
	    quantity-unit))

(define-record-type <quantity>
  (make-quantity value unit)
  quantity?
  (value quantity-value)
  (unit quantity-unit))
