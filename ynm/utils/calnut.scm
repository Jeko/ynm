(define-module (ynm utils calnut)
  #:use-module (ynm entities food)
  #:use-module (ice-9 i18n)
  #:use-module (ice-9 exceptions)
  #:export (calnut->food))

(define (calnut->food item)
  (if (list? item)
      (let* ([item-composition (apply make-composition
				      (map locale-string->inexact
					   (list-head (list-tail item 3) 62)))])
	(make-food
	 (list-ref item 0)
	 (list-ref item 1)
	 (list-ref item 2)
	 item-composition
	 (list-ref item 65)
	 (list-ref item 66)
	 (list-ref item 67)
	 (list-ref item 68)
	 (list-ref item 69)
	 (list-ref item 70)))
      "not a list"))
