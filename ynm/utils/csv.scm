(define-module (ynm utils csv)
  #:use-module (ice-9 textual-ports)
  #:use-module (ice-9 format)
  #:use-module (srfi srfi-1)
  #:export (csv-headers
	    csv-body))

(define (csv-headers filename)
  (parse-line (call-with-input-file filename get-line)))

(define (csv-body filename)
  (map parse-line
       (cddr (read-lines filename))))

(define (parse-line line)
  (let ([separator #\;])
    (string-split line (lambda (c) (equal? separator c)))))

(define (read-lines filename)
  (call-with-input-file filename
    (lambda (port)
      (let loop ([line ""] [lines '()])
	(if (eof-object? line)
	    (reverse lines)
	    (loop (get-line port) (cons line lines)))))))
