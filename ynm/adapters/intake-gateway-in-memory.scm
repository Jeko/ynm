(define-module (ynm adapters intake-gateway-in-memory)
  #:use-module (ynm entities intake-gateway)
  #:export (make-intake-gateway-in-memory
	    reset-intake-gateway-store)
  #:re-export (intake-gateway-create
	       intake-gateway-read
	       intake-gateway-update
	       intake-gateway-delete))

(define store '())

(define (make-intake-gateway-in-memory)
  (make-intake-gateway
   (lambda (intake) (set! store (cons intake store)))
   (lambda () store)
   (const #f)
   (const #f)))

(define (reset-intake-gateway-store)
  (set! store '()))
