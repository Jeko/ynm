(define-module (ynm adapters food-gateway-in-memory)
  #:use-module (ynm entities food-gateway)
  #:use-module (ynm entities food)
  #:export (make-food-gateway-in-memory)
  #:re-export (food-gateway-create
	       food-gateway-read
	       food-gateway-update
	       food-gateway-delete))

(define (make-food-gateway-in-memory)
  (let ([store '()])
    (make-food-gateway
     (lambda (food) (set! store (cons food store)))
     (lambda (label)
       (if label
	   (filter
	    (lambda (item)
	      (string-contains-ci (food-label item) label))
	    store)
	   store))
     (const #f)
     (const #f))))
